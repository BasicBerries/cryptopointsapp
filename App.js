import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import { HomeScreen } from './src/components/HomeScreen';
import { AmountScreen } from './src/components/AmountScreen';
import { BalanceScreen } from './src/components/BalanceScreen';
import { TransactionScreen } from './src/components/TransactionScreen';
import { TransactionHistoryScreen } from './src/components/TransactionHistoryScreen';

const RootStack = StackNavigator({
    Home: {
        screen: HomeScreen,
    },
    Amount: {
        screen: AmountScreen,
    },
    TransactionHistory: {
        screen: TransactionHistoryScreen,
    },
    Transaction: {
        screen: TransactionScreen,
    },
    Balance: {
        screen: BalanceScreen,
    },
});

export default class App extends Component {
    render() {
        return <RootStack />;
    }
}
