import { EthereumAccount } from "./EthereumAccount";

export function MockReader(web3) {
    let _customerPrivateKey = "0xfb726ad42b7157a047997289d001f8b47cd8bf57162fca5a22b239ccb851892b";
    EthereumAccount.call(this, web3, _customerPrivateKey);
}

MockReader.prototype = Object.create(EthereumAccount.prototype);
MockReader.prototype.constructor = MockReader;
