import EthereumTx from 'ethereumjs-tx';

export function EthereumAccount(web3, privateKey) {

    this._acc = web3.eth.accounts.privateKeyToAccount(privateKey);

    this.getPublicAddress = () => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this._acc.address);
            }, 1000);
        });
    }

    this.signTransaction = (txData) => {
        let signingKey = Buffer.from(this._acc.privateKey.split('x')[1], 'hex');
        let tx = new EthereumTx(txData);
        tx.sign(signingKey);
        let serializedTx = '0x' + tx.serialize().toString('hex');
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(serializedTx);
            }, 1000);
        });
    }
}
