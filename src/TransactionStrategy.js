export function TransactionStrategy(params) {
    this.getSenderPublicAddress = params.getSenderPublicAddress;
    this.getReceiverPublicAddress = params.getReceiverPublicAddress;
    this.signTransaction = params.signTransaction;
}
