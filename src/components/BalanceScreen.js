import React, { Component } from 'react';
import { Text, View } from 'react-native';

export class BalanceScreen extends Component {
    static navigationOptions = {
        title: 'Balance',
    };

    constructor(props) {
        super(props);
        this.state = {
            statusLog: 'Requesting sender public address.',
        };
    }

    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>
                    Balance: {this.state.balance}
                </Text>
                <View style={{padding: 30}} />
                <Text>{this.state.statusLog}</Text>
            </View>
        );
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;

        params.reader.getPublicAddress().then((val) => {
            this._handleReaderPublicAddress(val);
            return params.web3.eth.getBalance(this.state.readerPA);
        }).then((val) => {
            this._handleBalance(val);
        });
    }

    _handleReaderPublicAddress(readerPA) {
        this.setState((prevState) => {
            return {
                statusLog: prevState.statusLog +
                           '\nRequesting balance from node.',
                readerPA: readerPA,
            };
        });
    }

    _handleBalance(balance) {
        this.setState((prevState) => {
            return {
                statusLog: prevState.statusLog + '\nReceived balance.',
                balance: balance,
            };
        });
    }
}
