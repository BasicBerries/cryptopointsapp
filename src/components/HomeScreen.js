import React, { Component } from 'react';
import { Button, Switch, Text, View } from 'react-native';
import Web3 from 'web3';

import { MockReader } from '../MockReader';
import { EthereumAccount } from '../EthereumAccount';

export class HomeScreen extends Component {
    static navigationOptions = {
        title: 'CryptoPointsApp',
    };

    constructor(props) {
        super(props);

        let testmode = true;
        let publicNode = 'https://rinkeby.infura.io/zprh2wiMrkJp4p6fcq2W';
        let web3 = new Web3(new Web3.providers.HttpProvider(publicNode));
        this.state = {
            web3: web3,
            testmode: testmode,
            merchant: this._getMerchantAccount(web3, testmode),
            reader: this._getReaderAccount(web3, testmode),
        };
    }

    render() {
        return (
            <View style={{
                flex: 1,
            }}>
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                }}>
                    <Text>Test Mode</Text>
                    <Switch
                        onValueChange={(val) => {this._testmodeChanged(val)}}
                        value={this.state.testmode}
                    />
                </View>
                <View style={{
                    flex: 10,
                    justifyContent: 'space-around',
                    alignItems: 'center',
                }}>
                    <Button
                        title="Get Balance"
                        onPress={() => this.props.navigation.navigate('Balance', {
                            web3: this.state.web3,
                            merchant: this.state.merchant,
                            reader: this.state.reader,
                        })}
                    />
                    <Button
                        title="Send Points"
                        onPress={() => this.props.navigation.navigate('Amount', {
                            web3: this.state.web3,
                            merchant: this.state.merchant,
                            reader: this.state.reader,
                            action: 'send',
                        })}
                    />
                    <Button
                        title="Receive Points"
                        onPress={() => this.props.navigation.navigate('Amount', {
                            web3: this.state.web3,
                            merchant: this.state.merchant,
                            action: 'receive',
                            reader: this.state.reader,
                        })}
                    />
                    <Button
                        title="Transaction History"
                        onPress={() => this.props.navigation.navigate('TransactionHistory', {
                            web3: this.state.web3,
                            merchant: this.state.merchant,
                            reader: this.state.reader,
                        })}
                    />
                </View>
            </View>
        );
    }

    _testmodeChanged(testmode) {
        this.setState((prevState) => {
            return {
                testmode: testmode,
                merchant: this._getMerchantAccount(this.state.web3, testmode),
                reader: this._getReaderAccount(this.state.web3, testmode),
            };
        });
    }

    _getMerchantAccount(web3, testmode) {
        console.log("testmode=" + testmode);
        if (testmode) {
            return this._getTestMerchantAccount(web3);
        } else {
            return null;
        }
    }

    _getReaderAccount(web3, testmode) {
        if (testmode) {
            return this._getTestReader(web3);
        } else {
            return null;
        }
    }

    _getTestMerchantAccount(web3) {
        let _merchantPrivateKey = "0x7d78da5061abe78fba54372871a217838579a9fc9625bbb50fbb5d14c14a7e87";
        let acc = new EthereumAccount(web3, _merchantPrivateKey);
        return acc;
    }

    _getTestReader(web3) {
        return new MockReader(web3);
    }
}
