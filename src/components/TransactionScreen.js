import React, { Component } from 'react';
import { Text, View } from 'react-native';

export class TransactionScreen extends Component {
    static navigationOptions = {
        title: 'Transaction',
    };

    constructor(props) {
        super(props);
        this.state = {
            statusLog: 'Requesting sender public address.',
        };
    }

    render() {
        const { params } = this.props.navigation.state;
        const amount = params ? params.amount : null;
        const actionVerb = params ? params.action === 'send' ? 'Sending' :
                           'Receiving' : null;

        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>
                    {actionVerb} {amount} points.
                </Text>
                <View style={{padding: 30}}/>
                <Text>{this.state.statusLog}</Text>
            </View>
        );
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;

        // Start transaction process.
        params.txn.getSenderPublicAddress().then((val) => {
            this._handleSenderPublicAddress(val);
            return params.txn.getReceiverPublicAddress();
        }).then((val) => {
            this._handleReceiverPublicAddress(val);
            return params.web3.eth.getTransactionCount(this.state.senderPA);
        }).then((val) => {
            this._handleTransactionCount(val);
            return params.web3.eth.getGasPrice();
        }).then((val) => {
            this._handleGasPrice(val);
            return this._getSignedTransaction();
        }).then((val) => {
            this._sendSignedTransactionToBlockchain(val);
        });
    }

    _handleSenderPublicAddress(val) {
        this.setState((prevState) => {
            return {
                statusLog: prevState.statusLog +
                           "\nRequesting receiver public address.",
                senderPA: val,
            };
        });
    }

    _handleReceiverPublicAddress(val) {
        this.setState((prevState) => {
            return {
                statusLog: prevState.statusLog +
                           "\nRequesting sender nonce value.",
                receiverPA: val,
            };
        });
    }

    _handleTransactionCount(val) {
        this.setState((prevState) => {
            return {
                statusLog: prevState.statusLog +
                           "\nRequesting average gas price.",
                senderNonce: val,
            };
        });
    }

    _handleGasPrice(val) {
        this.setState((prevState) => {
            return {
                gasPrice: val,
            };
        });
    }

    _getSignedTransaction() {
        const { params } = this.props.navigation.state;

        this.setState((prevState) => {
            return {
                statusLog: prevState.statusLog + "\nSigning transaction.",
            };
        });
        let rawTx = {
            nonce: this.state.senderNonce,
            gasPrice: params.web3.utils.toHex(this.state.gasPrice),
            gasLimit: params.web3.utils.toHex('300000'),
            from: this.state.senderPA,
            to: this.state.receiverPA,
            value: params.web3.utils.toHex(params.amount),
            chainId: 4
        };
        return params.txn.signTransaction(rawTx);
    }

    _sendSignedTransactionToBlockchain(signedTxn) {
        const { params } = this.props.navigation.state;

        this.setState((prevState) => {
            return {
                statusLog: prevState.statusLog +
                           "\nSending transaction to blockchain.",
            };
        });
        params.web3.eth.sendSignedTransaction(signedTxn).on('receipt',
                                                            (val) => {
            this.setState((prevState) => {
                return {
                    statusLog: prevState.statusLog +
                               "\nFinished sending transaction.",
                };
            });
            console.log(val);
        });
    }
}
