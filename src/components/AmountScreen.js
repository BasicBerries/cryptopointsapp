import React, { Component } from 'react';
import { Button, Keyboard, Text, TextInput, View } from 'react-native';

import { MockReader } from '../MockReader';
import { EthereumAccount } from '../EthereumAccount';
import { TransactionStrategy } from '../TransactionStrategy';

export class AmountScreen extends Component {
    static navigationOptions = {
        title: 'Amount',
    };

    constructor(props) {
        super(props);
        this.state = {text: '0'};
    }

    render() {
        const { params } = this.props.navigation.state;
        const action = params ? params.action : 'unknown';

        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Text style={{padding: 10, fontSize: 20}}>
                    How many points?
                </Text>
                <TextInput
                    style={{height: 65, borderColor: 'gray', fontSize: 40}}
                    onChangeText={(text) => this.setState({text})}
                    keyboardType='numeric'
                />
                <View style={{padding: 10}}/>
                <Button
                    title={action}
                    onPress={() => {this._sendButtonPressed()}}
                />
            </View>
        );
    }

    _sendButtonPressed() {
        const { params } = this.props.navigation.state;


        Keyboard.dismiss();
        this.props.navigation.navigate('Transaction', {
            amount: this.state.text,
            action: params.action,
            txn: this._createTransactionStrategy(params.web3),
            web3: params.web3
        });
    }

    _createTransactionStrategy(web3) {
        const { params } = this.props.navigation.state;

        let txn;
        if (params.action === 'send') {
            txn = new TransactionStrategy({
                getSenderPublicAddress: params.merchant.getPublicAddress,
                getReceiverPublicAddress: params.reader.getPublicAddress,
                signTransaction: params.merchant.signTransaction
            });
        } else if (params.action === 'receive') {
            txn = new TransactionStrategy({
                getSenderPublicAddress: params.reader.getPublicAddress,
                getReceiverPublicAddress: params.merchant.getPublicAddress,
                signTransaction: params.reader.signTransaction
            });
        } else {
            console.fail("Invalid action: " + params.action);
        }

        return txn;
    }
}
